const faker = require('faker')

describe('set label on issue', () => {

    const issue = {
        title: `issue-${faker.random.uuid()}`,
        description: faker.random.words(5),
        project: {
            name: `project-${faker.random.uuid()}`,
            description: faker.random.words(5),
        }
    }

    const label = {
        name: `label-${faker.random.word()}`,
        color: '#ffaabb'
    }

    before(() => {
        cy.login()
        cy.api_createIssue(issue)
            .then(response => {
                cy.api_createLabel(response.body.project_id, label)
                cy.visit(`${Cypress.env('user_name')}/${issue.project.name}/issues/${response.body.iid}`)
            })
    })

    it('should set label with success', () => {
        cy.gui_setLabelOnIssue(label)
        cy.get('.qa-labels-block').should('contain', label.name)
        cy.get('.qa-labels-block span')
            .should(
                'have.attr',
                'style',
                `background-color: ${label.color}; color: #333333;`
            )
    });
})