describe('logout', () => {

    beforeEach(() => cy.login())

    it('logout successfully', () => {
        cy.logout()

        cy.url().should('be.equal', `${Cypress.config('baseUrl')}users/sign_in`)
    });
})