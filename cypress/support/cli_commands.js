Cypress.Commands.add('cloneViaSSH', project => {
    cy.exec(`cd temp/ && git clone git@localhost:${Cypress.env('user_name')}/${project.name}.git`)
})
