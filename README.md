# course-intermediary-cypress

Curso intermediário de cypress

# Anotações

é importe ter o controle sobre o ambiente de testes, onde conhecemos o estado da aplicação e não possui interferencia de terceiros.

## inicializa o package.json (sem o modo interativo)
package.json é um arquivo para gerenciar dependecias do projeto, e também é possível criar alguns scripts de teste
```
$ npm init -y
```

## intalar o cypress
```
$ npm i cypress -D
```

Quando executamos o comando `$ npx cypress open` o cypress de ante mão cria algumas coisas automáticamente para nós.
- cypress.json
- diretório cypress
- diretório fixtures
- diretório support
- diretório integration

### Aula 3 - Testes básicos de GUI (Graphic User Interface)
Fixtures - São dados que podemos criar para utilizar ao longo dos testes

Instalando a biblioteca faker
```
$ npm i faker -D
```

Cypress automaticamente faz a limpeza da sessão, por isso não precisamos utilizar como pré condição de outros testes o logout.

### Aula 4 - Testes intermediários de GUI
Os problemas de fazer tudo via GUI -
Os testes demoram demais para executar.
Os testes não são independentes, se um teste quebra, os outros não são executados

### Aula 5 - Testes de API
A ideia é prepapar o ambiente para testes chamando as API's e verificar se realmente foi criado algo na interface.

### Aula 6 - Otimizando testes de GUI
Apenas testando a velocidade da execução de testando quando executado via GUI e via API.
Via API é muito mais rápido.

### Aula 7 - Testes com muitas pré-condições
Testar via GUI apenas uma vez, então se eu já testei a criação de label via GUI, se algum outro teste depender disto, eu devo fazer a criação da label via API para otimizar o tempo de execução do teste.

### Aula 8 - Executando comandos a nível de sistema
É possível executar comandos de cli com o cypress, na aula testamos o comando `$ git clone`, e também verificamos se o README contém os dados que são pré preenchidos como nome e descrição.
